CREATE DATABASE IF NOT EXISTS gedima_gination_bdd;

use gedima_gination_bdd;

CREATE TABLE IF NOT EXISTS data (
    id_data INT NOT NULL AUTO_INCREMENT,
    debut_participation DATE NOT NULL,
    fin_participation DATE NOT NULL,
    debut_vote DATE NOT NULL,
    fin_vote DATE NOT NULL,
    reglement TEXT NOT NULL,
    classement_generer INT NOT NULL DEFAULT 0,
    classement_publique INT NOT NULL DEFAULT 0,

    CONSTRAINT pk_data PRIMARY KEY (id_data)
);

CREATE TABLE IF NOT EXISTS participation (
    id_participation INT(11) NOT NULL AUTO_INCREMENT,
    id_user INT(11) NOT NULL UNIQUE,
    image VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    titre VARCHAR(255) NOT NULL,
    date_creation DATE NOT NULL,
    vote INT(11) NOT NULL DEFAULT 0,
    place_classement INT(11),

    CONSTRAINT pk_images PRIMARY KEY (id_participation),
    CONSTRAINT fk_images_user FOREIGN KEY (id_user) REFERENCES auth_user(id)
);

CREATE TABLE IF NOT EXISTS action (
    id_action INT(11) NOT NULL AUTO_INCREMENT,
    action VARCHAR(255) NOT NULL,

    CONSTRAINT pk_action PRIMARY KEY (id_action)
);

CREATE TABLE IF NOT EXISTS consentement (
    id_consentement INT(11) NOT NULL AUTO_INCREMENT,
    id_user INT(11) NOT NULL,
    id_action INT(11) NOT NULL,
    date_consentement DATETIME NOT NULL,

    CONSTRAINT pk_consentement PRIMARY KEY (id_consentement),
    CONSTRAINT fk_consentement_user FOREIGN KEY (id_user) REFERENCES auth_user(id),
    CONSTRAINT fk_consentement_action FOREIGN KEY (id_action) REFERENCES action(id_action)
);