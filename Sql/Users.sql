
CREATE USER 'dev_gedima_gination'@'%' IDENTIFIED BY '';
CREATE USER 'prod_gedima_gination'@'%' IDENTIFIED BY '';

CREATE DATABASE IF NOT EXISTS gedima_gination_bdd;
CREATE DATABASE IF NOT EXISTS gedima_gination_bdd_test;

GRANT ALL PRIVILEGES ON gedima_gination_bdd.* TO 'dev_gedima_gination'@'localhost';
GRANT ALL PRIVILEGES ON gedima_gination_bdd_test.* TO 'dev_gedima_gination'@'localhost';

GRANT SELECT ON gedima_gination_bdd.* TO 'prod_gedima_gination'@'localhost';
GRANT INSERT ON gedima_gination_bdd.* TO 'prod_gedima_gination'@'localhost';
GRANT UPDATE ON gedima_gination_bdd.* TO 'prod_gedima_gination'@'localhost';
GRANT DELETE ON gedima_gination_bdd.* TO 'prod_gedima_gination'@'localhost';
GRANT SHOW VIEW ON gedima_gination_bdd.* TO 'prod_gedima_gination'@'localhost';
GRANT SELECT ON gedima_gination_bdd_test.* TO 'prod_gedima_gination'@'localhost';
GRANT INSERT ON gedima_gination_bdd_test.* TO 'prod_gedima_gination'@'localhost';
GRANT UPDATE ON gedima_gination_bdd_test.* TO 'prod_gedima_gination'@'localhost';
GRANT DELETE ON gedima_gination_bdd_test.* TO 'prod_gedima_gination'@'localhost';
GRANT SHOW VIEW ON gedima_gination_bdd_test.* TO 'prod_gedima_gination'@'localhost';