--Data
INSERT INTO data VALUES (1, '2022-03-11', '2022-05-09', '2022-05-16', '2022-06-16', '
<span>
    <p><span style="font-weight:bold;">Article 1 : Organisateur du jeu</span> La société NEGOMAT Ce jeu concours
        se déroule du 11/04/2022 au 09/05/2022 minuit inclus, dans les conditions prévues au
        présent règlement.
    </p>
    <p><span style="font-weight:bold;">Article 2 : Participation</span> Le jeu est ouvert aux clients de NEGOMAT. Pour
        participer, il suffit de s\’inscrire et poster une photo d’une réalisation faite à partir de matériaux achetés chez NEGOMAT.
    </p>
    <p><span style="font-weight:bold;">Article 3 : Annonce du jeu</span> La promotion et les informations relatives à
        l\’existence du jeu concours sont assurées via les sites internet LOCALHOST et e-mailing.
    </p>
    <p><span style="font-weight:bold;">Article 4 : Modalités de participation</span>
        <br><span style="font-weight:bold;">4.1</span> Pour participer au jeu (s\’inscrire et poster une photo), il faut être client
        chez NEGOMAT, professionnel ou particulier.
        <br><span style="font-weight:bold;">4.2</span>  Pour voter, il faut être client du magasin et avoir effectué un achat le jour
        du vote. Pour cela, un bon pour participer au jeu est remis à chaque client lors de son passage en caisse.
        Sur ce bon, figure un code unique à 12 caractères (Exemple : ACF2 FR77 1SG4) Chaque votant choisit ses trois
        réalisations préférées en attribuant à chacune un nombre de G\’aime compris entre 1 et 5.
        <br><span style="font-weight:bold;">4.3</span>Les trois ou plus en cas d\'égalité gagnants du jeu concours sont les participants dont la photo a
        récolté le plus grand nombre de G\’aime
    </p>
</span>', 0, 0);

-- Group
INSERT INTO `auth_group` (`id`, `name`) VALUES
	(2, 'admin'),
	(1, 'user');

-- Permission
INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
	(1, 2, 1),
	(2, 2, 2),
	(3, 2, 3),
	(4, 2, 4),
	(5, 2, 5),
	(6, 2, 6),
	(7, 2, 7),
	(8, 2, 8),
	(9, 2, 9),
	(10, 2, 10),
	(11, 2, 11),
	(12, 2, 12),
	(13, 2, 13),
	(14, 2, 14),
	(15, 2, 15),
	(16, 2, 16),
	(17, 2, 17),
	(18, 2, 18),
	(19, 2, 19),
	(20, 2, 20),
	(21, 2, 21),
	(22, 2, 22),
	(23, 2, 23),
	(24, 2, 24),
	(25, 2, 25),
	(26, 2, 26),
	(27, 2, 27),
	(28, 2, 28),
	(29, 2, 29),
	(30, 2, 30),
	(31, 2, 31),
	(32, 2, 32),
	(33, 2, 33),
	(34, 2, 34),
	(35, 2, 35),
	(36, 2, 36),
	(37, 2, 37),
	(38, 2, 38),
	(39, 2, 39),
	(40, 2, 40),
	(41, 2, 41),
	(42, 2, 42),
	(43, 2, 43),
	(44, 2, 44),
	(45, 2, 45),
	(46, 2, 46),
	(47, 2, 47),
	(48, 2, 48),
	(49, 2, 49),
	(50, 2, 50),
	(51, 2, 51),
	(52, 2, 52),
	(53, 2, 53),
	(54, 2, 54),
	(55, 2, 55),
	(56, 2, 56),
	(57, 2, 57),
	(58, 2, 58),
	(59, 2, 59),
	(60, 2, 60),
	(61, 2, 61),
	(62, 2, 62),
	(63, 2, 63),
	(64, 2, 64),
	(65, 2, 65),
	(66, 2, 66),
	(67, 2, 67),
	(68, 2, 68),
	(69, 2, 69),
	(70, 2, 70),
	(71, 2, 71),
	(72, 2, 72),
	(73, 2, 73),
	(74, 2, 74),
	(75, 2, 75),
	(76, 2, 76),
	(77, 2, 77),
	(78, 2, 78),
	(79, 2, 79),
	(80, 2, 80),
	(81, 2, 81),
	(82, 2, 82),
	(83, 2, 83),
	(84, 2, 84),
	(85, 2, 85),
	(86, 2, 86),
	(87, 2, 87),
	(88, 2, 88),
	(89, 2, 89),
	(90, 2, 90),
	(91, 2, 91),
	(92, 2, 92);

--Users
INSERT INTO auth_user (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES ('pbkdf2_sha256$320000$qKIluteqHojIut08z7Fzr8$iTg/Qn3k+1HF6lONC6PkwBprZMK+OgsqfH1ux4KiRcE=',1'Admin','Admin','Admin','admin@mail.com',1,1,'2022-05-03 11:40:08.941985');
INSERT INTO auth_user (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES ('pbkdf2_sha256$320000$qKIluteqHojIut08z7Fzr8$iTg/Qn3k+1HF6lONC6PkwBprZMK+OgsqfH1ux4KiRcE=',1,'Gestionnaire','Gestionnaire','Gestionnaire','gestionnaire@mail.com',1,1,'2022-05-03 11:40:08.941985');
INSERT INTO auth_user (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES ('pbkdf2_sha256$320000$qKIluteqHojIut08z7Fzr8$iTg/Qn3k+1HF6lONC6PkwBprZMK+OgsqfH1ux4KiRcE=',0'Admin','Admin','Admin','admin@mail.com',0,1,'2022-05-03 11:40:08.941985');

--Action
INSERT INTO action (action) VALUES ('Traitement des pour la création d un compte');
INSERT INTO action (action) VALUES ('Traitement des données pour la participation');

--Consentements


--Participation Sans vote
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('5', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('6', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('7', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('8', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('9', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('10', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('11', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('12', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('13', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('14', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('15', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('16', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('17', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('18', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('19', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('20', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('21', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');
INSERT INTO participation (id_user, image, description, titre, date_creation) VALUES ('22', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51');

--Participation Avec vote
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('5', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '68');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('7', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '200');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('6', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '156');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('8', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '225');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('9', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '25');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('10', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '200');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('11', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '58');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('12', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '56');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('13', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '40');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('14', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '154');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('15', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '168');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('16', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '10');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('17', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '56');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('18', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '158');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('19', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '168');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('20', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '200');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('21', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '225');
INSERT INTO participation (id_user, image, description, titre, date_creation, vote) VALUES ('22', 'test.jpg', 'description', 'titre', '2022-04-08 17:57:51', '58');
